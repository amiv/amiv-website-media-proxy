from nginx

WORKDIR /app
COPY ./ ./

COPY amiv-website-media-proxy.conf /usr/local/share/amiv-website-media-proxy.conf
COPY docker-entrypoint.sh /usr/local/bin/

CMD [ "docker-entrypoint.sh" ]
