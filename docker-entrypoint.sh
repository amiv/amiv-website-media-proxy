#!/bin/bash

AMIVAPI_URL="${AMIVAPI_URL:-"https://api-dev.amiv.ethz.ch"}"

yes | cp -rf /usr/local/share/amiv-website-media-proxy.conf /etc/nginx/conf.d/

sed -i "s|AMIVAPI_URL|${AMIVAPI_URL}|" /etc/nginx/conf.d/amiv-website-media-proxy.conf

nginx -g 'daemon off;'
