# AMIV Website Media Proxy

For the AMIV Website, next.js needs the environment variable `NEXT_PUBLIC_API_DOMAIN` on build time. However, the build for prod and dev does not differ and uses api-dev as a default. Thus, studydocs tries to get files from api-dev.

This proxy forwards all requests to /media to the correct api endpoint.
